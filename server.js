/*!
 *  description
 *   
 *  @package     server.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        path
 *  @since      1.0.0
 */


var http = require('http');
var app = require('./config/express')();
var config = require('./config/config')();

Object.assign=require('object-assign')

require('./config/database')(config.db);

app.listen(config.port, config.address)

console.log('Express server escutando na porta: ', config.port);

module.exports = app ;