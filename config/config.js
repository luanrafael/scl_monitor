/*!
 *  arquivo de configuração
 *   
 *  @package     config
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        /config
 *  @since       0.0.1
 */


 module.exports = () => {


 	return require('./env/production.js');
 	//return require('./env/development.js');


 };
  
