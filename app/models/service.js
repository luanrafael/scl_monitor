/*!
 *  model user
 *   
 *  @package     /models
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        /models/service.js
 *  @since       1.0.0
 */


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports = () => {

	var service_model = {
		name: {
			type: String,
			required: true
		},
		created: {
			type: Date,
			default: Date.now
		},
		updated: {
			type: Date,
			default: Date.now
		},
		config: {
			type: Schema.Types.Mixed
		},
		status: {
			type: Number,
			default: 1
		},
		type: {
			type: String,
			required: true
		},
		qtd_errors: {
			type: Number,
			default: 0
		},
		alert: {
			type: Number,
			default: 2
		},
	}

	var schema = mongoose.Schema(service_model);

	return mongoose.model('Service', schema)
};
 