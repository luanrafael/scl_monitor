var request = require('request');

module.exports = () => {

	var monitor = {};

	monitor.verify = (register, callback) => {
		register.config.timeout = 1500;
		request(register.config,function(err, resp, body) {

			if(err){
				callback(err, {id: register.id, error: false});
			} 
			else {
				var output = {id : register.id}
				output.error = resp.statusCode >=  400;
				callback(null, output);
			}

		});

	}

	return monitor;

};
