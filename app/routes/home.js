/*!
 * route /home
 *   
 *  @package     routes/home.js
 *  @author      Luan Rafael <luanrafaelcastor@gmail.com>
 *  @copyright   lrpinheiro.tk
 *  @link        routes/home.js
 *  @since      1.0.0
 */


module.exports = (app) => {

	var controller = app.controllers.home;

	app.route('/render/:view').get(controller.render);
	app.route('/teste').get(controller.teste);
	app.route('/index').get(controller.index);
	app.route('/home').get(controller.index);
	app.route('/').get(controller.index);

}